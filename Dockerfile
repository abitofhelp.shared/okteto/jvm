# This Dockerfile creates a container for JVM development in Okteto.
# It includes the following components:
#    AdoptOpenJdk
#    Gradle
#    Kotlin
#    Kubectl CLI
#    Okteto CLI
#    Pack
#    Python
#    Ubuntu
#
# Project:
#    https://gitlab.com/abitofhelp.shared/okteto/jvm.git
# Docker:
#    build -t abitofhelp/okteto-jvm:latest -t abitofhelp/okteto-jvm:1.0.2 .
#    docker pull abitofhelp/okteto-jvm

# STAGE1: UBUNTU BASE IMAGE
ARG UBUNTU_VERSION=18.04
FROM ubuntu:${UBUNTU_VERSION} as aboh-upgrade
WORKDIR "/opt"

LABEL version="1.0.2"
LABEL maintainer="support@abitofhelp.com"
LABEL license=" \
MIT License \
 \
Copyright (c) 2020 A Bit of Help, Inc. \
 \
Permission is hereby granted, free of charge, to any person obtaining a copy \
of this software and associated documentation files (the 'Software'), to deal \
in the Software without restriction, including without limitation the rights \
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell \
copies of the Software, and to permit persons to whom the Software is \
furnished to do so, subject to the following conditions: \
 \
The above copyright notice and this permission notice shall be included in all \
copies or substantial portions of the Software. \
 \
THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR \
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, \
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE \
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER \
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, \
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE \
SOFTWARE. \
"

RUN DEBIAN_FRONTEND=noninteractive apt -y update && apt -y full-upgrade

# STAGE2: MISCELLANEOUS ADD-ONS
FROM aboh-upgrade as aboh-addons
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    ca-certificates curl fontconfig git gnupg2 gpg-agent lsb-release locales openssl \
    apt-transport-https software-properties-common tzdata unzip wget \
    && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen en_US.UTF-8 \
    && rm -rf /var/lib/apt/lists/*

# STAGE3: AZURE
FROM aboh-addons as aboh-azure
##ARG AZURE_VERSION=installs the latest version
#ENV AZURE_HOME=/opt/az
#ENV PATH=${AZURE_HOME}/bin:${PATH}
#RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

# STAGE4: PYTHON 3.X
FROM aboh-azure as aboh-python
ARG PYTHON_VERSION=3.9
RUN DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:deadsnakes/ppa \
&& apt-get install -y --no-install-recommends python${PYTHON_VERSION}

# STAGE5: JDK
FROM aboh-python as aboh-jdk
ARG JAVA_VERSION=jdk-11.0.8+10
ENV JAVA_HOME=/opt/java/openjdk
ENV PATH="/opt/java/openjdk/bin:$PATH"
RUN set -eux; \
    ARCH="$(dpkg --print-architecture)"; \
    case "${ARCH}" in \
       aarch64|arm64) \
         ESUM='fb27ea52ed901c14c9fe8ad2fc10b338b8cf47d6762571be1fe3fb7c426bab7c'; \
         BINARY_URL='https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.8%2B10/OpenJDK11U-jdk_aarch64_linux_hotspot_11.0.8_10.tar.gz'; \
         ;; \
       armhf|armv7l) \
         ESUM='d00370967e4657e137cc511e81d6accbfdb08dba91e6268abef8219e735fbfc5'; \
         BINARY_URL='https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.8%2B10/OpenJDK11U-jdk_arm_linux_hotspot_11.0.8_10.tar.gz'; \
         ;; \
       ppc64el|ppc64le) \
         ESUM='d206a63cd719b65717f7f20ee3fe49f0b8b2db922986b4811c828db57212699e'; \
         BINARY_URL='https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.8%2B10/OpenJDK11U-jdk_ppc64le_linux_hotspot_11.0.8_10.tar.gz'; \
         ;; \
       s390x) \
         ESUM='5619e1437c7cd400169eb7f1c831c2635fdb2776a401147a2fc1841b01f83ed6'; \
         BINARY_URL='https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.8%2B10/OpenJDK11U-jdk_s390x_linux_hotspot_11.0.8_10.tar.gz'; \
         ;; \
       amd64|x86_64) \
         ESUM='6e4cead158037cb7747ca47416474d4f408c9126be5b96f9befd532e0a762b47'; \
         BINARY_URL='https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.8%2B10/OpenJDK11U-jdk_x64_linux_hotspot_11.0.8_10.tar.gz'; \
         ;; \
       *) \
         echo "Unsupported arch: ${ARCH}"; \
         exit 1; \
         ;; \
    esac; \
    curl -LfsSo /tmp/openjdk.tar.gz ${BINARY_URL}; \
    echo "${ESUM} */tmp/openjdk.tar.gz" | sha256sum -c -; \
    mkdir -p /opt/java/openjdk; \
    cd /opt/java/openjdk; \
    tar -xf /tmp/openjdk.tar.gz --strip-components=1; \
    rm -rf /tmp/openjdk.tar.gz;

# STAGE6: OKTETO
FROM aboh-jdk as aboh-oktetocli
#ARG OKTETO_VERSION=installs the latest version
ENV OKTETO_HOME=/opt/okteto
ENV PATH=${OKTETO_HOME}/bin:${PATH}
RUN mkdir -p ${OKTETO_HOME}/bin \
&& cd ${OKTETO_HOME}/bin \
&& curl https://get.okteto.com -sSfL | sh \
&& mv /usr/local/bin/okteto .

# STAGE7: CLOUD NATIVE PACK
FROM aboh-oktetocli as aboh-pack
ARG PACK_VERSION=0.14.2
ENV PACK_HOME=/opt/pack
ENV PATH=${PACK_HOME}/bin:${PATH}
RUN mkdir -p ${PACK_HOME}/bin \
&& curl -sSL https://github.com/buildpacks/pack/releases/download/v${PACK_VERSION}/pack-v${PACK_VERSION}-linux.tgz | tar -C ${PACK_HOME}/bin --no-same-owner -xzv pack

# STAGE8: KOTLIN
FROM aboh-pack as aboh-kotlin
ARG KOTLIN_VERSION=1.4.10
ENV KOTLIN_HOME=/opt/kotlinc
ENV PATH=${KOTLIN_HOME}/bin:${PATH}
RUN wget -q https://github.com/JetBrains/kotlin/releases/download/v${KOTLIN_VERSION}/kotlin-compiler-${KOTLIN_VERSION}.zip \
&& unzip *kotlin*.zip  \
&& rm *kotlin*.zip

# STAGE9: GRADLE
FROM aboh-kotlin as aboh-gradle
ARG GRADLE_VERSION=6.7
ARG GRADLE_DIST=bin
ENV GRADLE_HOME=/opt/gradle-6.7
ENV PATH=${GRADLE_HOME}/bin:${PATH}
RUN wget -q https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-${GRADLE_DIST}.zip \
&& unzip *gradle*.zip \
&& rm *gradle*.zip

# STAGE10: KUBECTL
FROM aboh-gradle as aboh-kubectl
#ARG KUBECTL_VERSION=installs the latest version
ENV KUBECTL_HOME=/opt/kubectl
ENV PATH=${KUBECTL_HOME}/bin:${PATH}
WORKDIR ${KUBECTL_HOME}/bin
RUN curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" \
&& chmod +x kubectl

# DONE
FROM aboh-kubectl as aboh-done
WORKDIR /
CMD ["bash"]